#!/bin/sh -e

# Enable udev OpenRC services and runlevels
# https://github.com/alpinelinux/aports/blob/master/main/eudev/setup-udev
setup-udev -n

# Enable other OpenRC services
for service in devfs dmesg devmappings; do
	rc-update -q add $service sysinit
done
for service in hwclock modules sysctl hostname bootmisc syslog; do
	rc-update -q add $service boot
done
for service in dbus haveged sshd swapfile wpa_supplicant chronyd local networkmanager; do
	rc-update -q add $service default
done
for service in mount-ro killprocs savecache; do
	rc-update -q add $service shutdown
done

# Prime swclock with a reasonable date/time on first boot
mkdir -p /run/openrc
touch /run/openrc/shutdowntime

# Install /etc/fstab
if ! grep -q "postmarketos\.org/fstab" /etc/fstab; then
	echo "- Modifying: /etc/fstab"
	{
		echo
		echo "# This file is *not* used to mount / or /boot."
		echo "# More information: https://postmarketos.org/fstab"
	} >> /etc/fstab
fi

# Set /etc/issue
echo "- Modifying: /etc/issue"
{
	echo 'Welcome to postmarketOS'
	echo 'Kernel \r on an \m (\l)'
	# setterm -powersave on -blank 5
	echo -ne "\033[9;5]"
} >/etc/issue

# Set /etc/motd
echo "- Modifying: /etc/motd"
{
	echo 'Welcome to postmarketOS!'
	echo ''
	echo 'This distribution is based on Alpine Linux.'
	echo 'Read both our wikis to find a large amount of how-to guides and'
	echo 'general information about administrating and development.'
	echo 'See <https://wiki.postmarketos.org> and <https://wiki.alpinelinux.org>.'
	echo ''
	echo 'You may change this message by editing /etc/motd.'
	echo ''
} >/etc/motd

# Enable syslog to SHM. Note: size is 4048KiB, which *should* be More Than Enough(TM) for all but the most chattiest of devices.
echo "- Modifying: /etc/conf.d/syslog"
sed s/=\"/=\""-C4048 "/  -i /etc/conf.d/syslog

# Enable nl80211,wext & dbus control for wpa_supplicant
if ! grep -q Dnl80211,wext /etc/conf.d/wpa_supplicant; then
	echo "- Modifying: /etc/conf.d/wpa_supplicant"
	sed -i 's/wpa_supplicant_args=\"/wpa_supplicant_args=\" -u -Dnl80211,wext/' \
		/etc/conf.d/wpa_supplicant
fi
touch /etc/wpa_supplicant/wpa_supplicant.conf

# Enable the 'wheel' group
echo "- Modifying: /etc/sudoers"
sed -i 's/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/sudoers

# Set chrony to quickly correct system time whenever it differs from
# ntp time by more than 1s.
if ! grep -q "makestep" /etc/chrony/chrony.conf; then
	echo "- Modifying: /etc/chrony/chrony.conf (adding makestep)"
	echo "makestep 1 -1" >> /etc/chrony/chrony.conf
fi

# Comment out initstepslew in the chrony configuration to avoid a delay 
# during boot. 
if grep -q "^initstepslew" /etc/chrony/chrony.conf; then
	echo "- Modifying: /etc/chrony/chrony.conf (removing initstepslew)"
	sed -i 's/^initstepslew/#initstepslew/' /etc/chrony/chrony.conf
fi

exit 0
